<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 06.05.2017
 * Time: 1:35
 */

namespace frontend\controllers;


class PostController extends AppController
{
    public function actionHello()
    {
        $hello = 'Hello World!';
        return $this->render('Test', ['hello'=>$hello]);
    }
}