<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 05.05.2017
 * Time: 23:40
 */

namespace backend\controllers;


class PostController extends AppController
{

    public function actionTest()
    {
        $hello = 'Hello World!';
        return $this->render('test', ['hello'=>$hello]);
    }
}